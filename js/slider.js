function initTabs() {
	// Img
	const imgSlider = document.querySelector('.slider-img');
	const imgTrack = document.querySelector('.slider-img__track');
	const imgSlides = document.querySelectorAll('.slider-img__slide');

	// Content
	const contentSlider = document.querySelector('.slider-content');
	const contentTrack = document.querySelector('.slider-content__track');
	const arrContentSlides = document.querySelectorAll('.slider-content__slide');

	const buttonTabs = document.querySelectorAll('.tabs__header-item');

	let index = 0;
	let sliderImgWidth;
	let sliderContentWidth;

	const init = () => {
		sliderImgWidth = imgSlider.offsetWidth;
		sliderContentWidth = contentSlider.offsetWidth;

		const imgTrackWidth = sliderImgWidth * imgSlides.length;
		const contentTrackWidth = sliderContentWidth * arrContentSlides.length;

		imgTrack.style.width = imgTrackWidth + 'px';
		contentTrack.style.width = contentTrackWidth + 'px';

		imgSlides.forEach(slide => {
			slide.style.width = sliderImgWidth + 'px';
			slide.style.height = 'auto';
		});

		arrContentSlides.forEach(slide => {
			slide.style.width = sliderContentWidth + 'px';
			slide.style.height = 'auto';
		});
	};

	const draftTract = () => {
		const offsetImg = sliderImgWidth * index;
		const offsetContent = sliderContentWidth * index;

		imgTrack.style.transform = `translateX(-${offsetImg}px)`;
		contentTrack.style.transform = `translateX(-${offsetContent}px)`;
	};

	buttonTabs.forEach(tab => {
		tab.addEventListener('click', event => {
			const className = 'tabs__header-item_active';
			const tabIndex = Number(event.target.dataset.index);

			buttonTabs.forEach(tab => tab.classList.remove(className));
			event.target.classList.add(className);
			index = tabIndex;

			draftTract();
		});
	});

	window.addEventListener('resize', init);

	init();
}

export default initTabs;
